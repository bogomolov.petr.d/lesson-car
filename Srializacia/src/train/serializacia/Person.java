package train.serializacia;

import java.io.Serializable;

public class Person implements Serializable {

    private static final long serialVersionUID = 6496319683714448584L;
    private  int id;
    private  String personName;


    public Person(int id, String personName) {
        this.id = id;
        this.personName = personName;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return personName;
    }

    @Override
    public String toString() {
        return id + " : " + personName;
    }
}
