package train2;

import java.io.Serializable;

public class Game  implements Serializable {

    private static final long serialVersionUID = -1973752682884757468L;
    private String country;
    private String klan;
    private int pointOfKlan;

    public Game(String country, String klan, int pointOfKlan) {
        this.country = country;
        this.klan = klan;
        this.pointOfKlan = pointOfKlan;
    }

    public String getKlan() {
        return klan;
    }

    public int getPointOfKlan() {
        return pointOfKlan;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public String toString() {
        return country + " : " + klan + " : " + pointOfKlan;
    }
}
