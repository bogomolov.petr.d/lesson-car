package train2;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class WriteObjGame {
    public static void main(String[] args) throws IOException {

        Game[] versions = {new Game("USA", "Tramp", 34),
                            new Game("Germani", "Merkel", 28),
                             new Game("Russia", "Putin", 12)};


        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("versions.bin"));

        oos.writeObject(versions);
    }
}
