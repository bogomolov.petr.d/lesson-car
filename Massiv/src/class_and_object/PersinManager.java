package class_and_object;

public class PersinManager {
    public static void main(String[] args) {
        Man person = new Man();
        person.setAge(-1);
        person.getAge();
        person.speak();


    }
}

class Man {
    private int age;
    private String name;//my comment
    private char gender;

    void speak () {
        System.out.println(age);
    }


    public void setAge(int userAge) {
        if(userAge < 0 ) {
            System.out.println("no correct");
        } else {
            age = userAge;
        }
    }

    public int getAge() {
        return age;
    }


}

