package practic;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Arrays;

public class ReadToFile {

    public static void main(String[] args) throws FileNotFoundException {

        File file = new File( "C:/Users/Acer/Desktop/test.java");

        Scanner scanner = new Scanner(file);
        String line = scanner.nextLine();
        String[] words = line.split(" ");


        System.out.println(Arrays.toString(words));

        scanner.close();
    }
}
