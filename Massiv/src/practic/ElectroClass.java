package practic;

public class ElectroClass {

    private String name;

    private class Motor {
        public void starMotor() {
            System.out.println("Motor " + name +  " is starting");
        }
    }
    public static class Battery {
        public void charge() {
            System.out.println("A battery is cgarging");
        }
    }

    public ElectroClass(String name) {
        this.name = name;
    }

    public void start() {
        Motor motor = new Motor();
        motor.starMotor();
        System.out.println("Eloctro car " + name + " is stasrung");
    }

    public void change() {
        Battery battery = new Battery();
        battery.charge();
    }


}
