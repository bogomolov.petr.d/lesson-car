package practic;

import java.util.Arrays;

import java.util.OptionalInt;

public class Massiv {
    public static void main(String[] args) {


        int[] nums = {13, 34, 546, 67, 8, 9, 78, 34, 45, 76, 7, 889, 2};
        OptionalInt max = Arrays.stream(nums).max();
        System.out.println(max.getAsInt());

        OptionalInt min = Arrays.stream(nums).min();
        System.out.println(min.getAsInt());

        System.out.println();

        int[]numbers = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
        for(int i =0; i < numbers.length; i++) {
            System.out.println(numbers[i]);

        }

    }
}
