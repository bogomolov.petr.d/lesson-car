package practic;

public class Words {

    public static void main(String[] args) {

        String offer = "Hello my wonderful world and goodbuy!";

        String[] words = offer.split(" "); //Создаем массив из слов фразы
        System.out.println("Количество слов: " +  words.length);

        System.out.println(); //для пробела в консоле

            for(int i = 0; i < words.length; i++) {  //делаем вывод каждого слова по очереди

                char[] letters = words[i].toCharArray(); //формируем массив букв из слов
                System.out.println((words[i]) + " " + "в этом слове: " + letters.length + " " + "букв");
        }

    }
}
