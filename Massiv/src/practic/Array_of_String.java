package practic;

public class Array_of_String {
    public static void main(String[] args) {

        String[] words = {"Hi", "Mister", "Petr"};

        for(String str:words) {
            System.out.println(str);
        }

        int[] numbers = {1,3,4,6,7};
        for(int i:numbers) {
            System.out.println(i);
        }

    }
}
