package carmanager;

public class FamilyCar extends Car {

    public FamilyCar(Model model, int yearOfProduction, int price, int weight, Color color, NameDriver driver) {
        super(model, yearOfProduction, price, weight, color, driver);
    }

    @Override
    public boolean isReadyToServise() {
        if(distanceOnService > 15000) {
            return  true;
        }
            else {
                return false;
            }
        }

    @Override
    public void addDistance(int additinalDistance, int distance) {
        this.distance = this.distance + additinalDistance;
        distanceOnService = distanceOnService + additinalDistance;
    }
}

